# 安装环境

## 基础环境

* JDK: 1.8+
* MAVEN: 3.3.9+
* MYSQL: 5.7+
* Redis: 3.0+
* RabbitMQ：3.7.2 （Erlang 20.1.7）

## IDE
IDEA 2017.3.2+
Lombok插件
